\documentclass[11pt, a4paper]{article}
\usepackage{geometry}
\geometry{a4paper, portrait, margin=1.2in}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{caption}
\captionsetup{justification   = raggedright,
              singlelinecheck = false}
\usepackage[none]{hyphenat}
\usepackage{natbib}
\usepackage{booktabs}


\title{\textbf{Broadscale model of 'More Birds in the Bush'}}
\author{Dean Anderson$^*$, Rachell Binny, Adrian Monks, Susan Walker}
\begin{document}
\maketitle

\begin{flushleft}

\vspace{0.5cm}

\begin{large}
\textbf{Manaaki Whenua Landcare Research}\\
$^*$andersond@landcareresearch.co.nz
\end{large}
\end{flushleft}

\vspace{0.5cm}





\noindent
\textbf{Subscripting:}

\begin{enumerate}
\item[\textit{i}:] grid cell for masting, rodent and bird processes (resol. 200 m)
\item[\textit{j}:] grid cells in neighbourhood window around cell \textit{i}
\item[\textit{k}:] grid cell for stoat processes (resol. 1000 m)
\item[\textit{t}:] Monthly time step for 70 years
\end{enumerate}

\noindent
\textbf{Forest Productivity (Masting) process}\\
The following procedure and equations are written for beech masting events, but they can be easily modified to accomodate spatial temporal variation in productivity in warm forests.

A masting or elevated productivity event ($MEvent_t$) occurs at the broadscale following a Bernouilli process with probability \textit{pMEvent}:

\begin{equation}
MEvent_t \sim \operatorname{Bern} \left({pMEvent}\right)
\end{equation}

\noindent
where \textit{pMEvent} is the annual probability of a masting event (\textit{e.g.} 0.175) and set in the params. 

Given a masting event, the proportion of cells masting is $pM_t$, which is a random variate from a \textit{Beta(a, b)}. The number of cells masting at time \textit{t} $(nM_t)$ is:

\begin{equation}
nM_t = nBeechCells * pM_t
\end{equation}

\noindent
where \textit{nBeechCells} is the total number of cells with beech forest cover. Again, the number of cells could be of a different cover type. We will define the spatially and temporally variable landscape elsewhere.

We use the following process to distribute the $nM_t$ cells across the landscape with spatial aggregation. We generate random relative-risk values for \textit{nBeechCells} and incorporated spatial autocorrelation. We draw random ‘risk’ variates for each grid cell from a gamma distribution with shape and scale parameters of a and b (to be set in params), respectively. 

We then loop through all grid cells two times. For each grid cell \textit{i} in the first loop we calculate the maximum random relative risk value of all cells in a square window centred on grid cell \textit{i} (window size set as number of pixels on a side in params; \textit{e.g.} 130 pixels). A random variate, $rv_{i,t}$, is then drawn from a normal distribution for grid cell \textit{i} as follows to ensure a positive value:

\begin{equation}
\log(rv_{i,t}) \sim Normal (\log(maxRisk_{i,t}), varMast)
\end{equation}

\noindent
Where \textit{varMast} is the variance set in params. We loop through all grid cells a second time, and for a given grid cell \textit{i} calculate the weighted average of all $rv_{j,t}$ among the \textit{J} cells in a square window centred on grid cell \textit{i}. Grid cells \textit{j} within the window were weighted by their distance to the centre grid cell i:

\begin{equation}
wt_j = \exp\left(\frac{-d^2_{i,j}}{2\rho^2}\right)
\end{equation}


\noindent
where $d^2_{i,j}$ is the squared distance between cell \textit{i} and \textit{j}, and $\rho$ is a spatial aggregation parameter (set in params; e.g. 16000.0). The $nM_t$ grid cells with the highest weighted average risk values are assigned a grid-cell mast event $M_{i,t}$.\\




\noindent
\textbf{Rodent processes}\\

We model the rodent population in cell \textit{i} with a mechanistic approach that decomposes monthly changes in a local population to the number of births and deaths ~\citep{Turchin2003}. The following is a very rough and initial approach. We can revise this with time and more careful thought.





Ricker discrete-time density dependent population model with a population growth rate, $r^{[R]}$, carrying capacity, $K^{[R]}_i$, for the habitat class in cell \textit{i}. $K^{[R]}_{i,t}$ is dependent on the habitat class and the resolution of the model, and may vary with time \textit{t} because of masting events. The superscript \textit{[R]} indicates rodents, because stoat and kiwi processes will also have the same parameters and variables. The user defines the carrying capacity for each class as individuals per hectare $(cc_i)$. Example $cc_i$ values are shown here:\\

\begin{tabular}{|c|c|c|c|c|c|}
\hline
Habitat & GrassScrub & Shrub & OtherForest & Beech & CoastBeech \\ \hline
$cc_i$ & 2.0 & 3.0 & 4.0 & 5.0 & 7.0\\ \hline
\end{tabular}\\

\noindent
If a masting event occurred in the preceding year (\textit{t-1}), a user-defined maximum carrying capacity (\textit{ccMast}; \textit{e.g.} 20 individuals per hectare) is applied to beech forest types.

The $K^{[R]}_{i,t}$ is then calculated using the specified resolution of the model as follows:

\begin{equation}
K^{[R]}_{i,t} = \left(\frac{resolution}{100}\right)^2 * cc_i
\end{equation}


We set an initial rodent population density at $t=0$ ($R_{i,0}$) for all grid cells using random variate from a Poisson distribution with a mean equal to 0.5 $K^{[R]}_{i,0}$:

\begin{equation}
R_{i,0} \sim Poisson(0.75 * K^{[R]}_{i,0})
\end{equation}

Prior to calculating $R_{i,t}$ with a population growth model (details below), we estimate the 'reproductive rodent density' $RR_{i,t}$ by adjusting the $R_{i,t-1}$ by incorporating immigration/emigration that occurred at the end of time \textit{t-1}, and control in time \textit{t} (if it occurred). First immigration and emigration are accounted for as follows:

\begin{equation}
RR_{i,t} = R_{i,t-1} + Imm^{[R]}_{i,t-1} - Em^{[R]}_{i,t-1}
\end{equation}

\noindent
where $Imm^{[R]}_{i,t-1}$ and $Em^{[R]}_{t-1}$ are the number of immigrants into and emigrants out of cell \textit{i}, respectively, at time \textit{t-1}. If a 1080 operation occurs in cell \textit{i}, the number of rodents eating toxin and becoming 'toxic rodents' $(T^{[R]}_{i,t})$ is random variate from a binomial distribution:

\begin{equation}\label{eq:toxicRodent}
T^{[R]}_{i,t} \sim Binomial(RR_{i,t}, \, pT^{[R]}_{i,t}C_{i,t})
\end{equation}

\noindent
where $pT^{[R]}_{i,t}$ is the probability of a rodent eating toxic bait and $C_{i,t}$ is a binary 1080 control variable in cell \textit{i} and year \textit{t} determined by the the management strategy, (not discussed here, yet). The subsequent reduction of the reproductive rodent population is calculated as follows:

\begin{equation}
RR_{i,t} = RR_{i,t} - T^{[R]}_{i,t}
\end{equation}


Population growth occurs after control in year \textit{t}. The estimated rodent population density in cell \textit{i} at time \textit{t} is drawn from a Poisson distribution with a mean from a Ricker population growth model:

\begin{equation}\label{eq:rodentPoisson}
R_{i,t} \sim Poisson (\mu^{[R]}_{i,t})
\end{equation}

\begin{equation}\label{eq:rodentMU}
\mu^{[R]}_{i,t} =  RR_{i,t}\exp \left(r^{[R]}\left(1 - \frac{RR_{i,t}}{K^{[R]}_{i,t}}\right)\right)
\end{equation}

The number of rodents that emigrate out of cell \textit{i} and year \textit{t} ($nEm^{[R]}_{i,t}$) is a random variate from a binomial distribution:

\begin{equation}
nEm^{[R]}_{i,t} b\sim Binomial(R_{i,t}, \, pEm^{[R]}_{i,t})
\end{equation}

\noindent
where $pEm^{[R]}_{i,t}$ is the probability that an individual will emigrate from cell \textit{i} in year \textit{t}. The $pEm^{[R]}_{i,t}$ will depend on the current population density $(R_{i,t})$ relative to $K^{[R]}_{i,t}$ so that the probability will increase with increasing density (Figure \ref{fig:pEmigraion}). 

\begin{equation}
pEm^{[R]}_{i,t} = 1.0 - \exp\left(-\gamma^{[R]}\frac{R_{i,t}}{K^{[R]}_{i,t}}\right)
\end{equation}

\noindent
where $\gamma^{[R]}$ is a rate parameter set in params (\textit{e.g.} 0.30).

The $nEm^{[R]}_{i,t}$ emigrants are then distributed into a window of \textit{J} cells that are in neighbourhood cells around \textit{i}. The n x n dimensions of the window are determined by the resolution and the maximum dispersal distance  of rodents ($maxDisp^{[R]}$). The destination cell \textit{j} of emigrants is determined by the distance from cell \textit{i} and the habitat quality or carrying capacity in cell \textit{j} $(K^{[R]}_{j,t})$. The $nEm^{[R]}_{i,t}$ are distributed with a multinomial process:

\begin{equation}
nEm^{[R]}_{i,j,t} \sim multinomial(nEm^{[R]}_{i,t}, \, pEm^{[R]}_{i,j,t})
\end{equation}

\noindent
where $nEm^{[R]}_{i,j,t}$ is the number of emigrants from cell \textit{i} into cell \textit{j} and $pEm^{[R]}_{i,j,t}$ is the probability of a disperser emigrating from cell \textit{i} into cell \textit{j} (note the addition of the \textit{j} subscript). 

The $pEm^{[R]}_{i,j,t}$ is calculated in a two-step process. First, the relative probability of emigrating from \textit{i} to \textit{j} is calculated as a function of distance between the cells $(d_{i,j})$ and the density ($R_{j,t}$) relative to $K^{[R]}_{j,t}$ (Figure \ref{fig:relProbEmig}):

\begin{equation}
relProbEm^{[R]}_{i,j,t} = \exp \left(-\frac{\tau^{[R]} R_{j,t}}{K^{[R]}_{j,t}} \right) \exp\left(-\frac{d_{i,j}}{\delta^{[R]}}\right)
\end{equation}

\noindent
where $\delta^{[R]}$ is a decay parameter describing the decreasing probability of immigrating into cell \textit{j} with increasing distance (\textit{e.g.} $\approx$ 0.25 of maximum dispersal distance), and $\tau^{[R]}$ is a rate parameter (\textit{e.g.} 1.4). Second, the absolute probability of emigrating from cell \textit{i} into cell \textit{j} is calculated by normalising all the $relProbEm^{[R]}_{i,j,t}$ of \textit{J} cells so that they sum to one:

\begin{equation}
pEm^{[R]}_{i,j,t} = \frac{relProbEm^{[R]}_{i,j,t}}{\sum_{j=1}^{J} relProbEm^{[R]}_{i,j,t}}
\end{equation}





 
\noindent
\textbf{Stoat population process}\\

We model the stoat population at a coarser resolution than for rodents (\textit{e.g.} 1000 m) so that the rodent-population grid cells are nested within the stoat raster. The stoat population is also modelled by grid cell (\textit{k}) with a logistic density dependent population model with population growth rate $r^{[S]}_{max}$ (\textit{e.g.} 4.5). The carrying capacity ($K^{[S]}_{k,t}$) is determined by the corresponding rodent population density $R_{k,t}$ (\textbf{note the \textit{k} in subscript}), which is the mean $R_{i,t}$ calculated over the rodent-resolution grid cells in grid cell \textit{k} (at the stoat resolution). The result is a sigmoidal relationship between $R_{k,t}$ and $K^{[S]}_{k,t}$ (Figure \ref{fig:stoatK}).


The $K^{[S]}_{k,t}$ is calculated with the following generalised logistic function:

\begin{equation}\label{eq:genLogistic}
K = A_{min} + \frac{A_{max} - A_{min}}{\left(1 + Qe^{-\omega(R_{i,t} - L)}\right)^{\frac{1}{\nu}}}
\end{equation}

\noindent
where $A_{min}$ and $A_{max}$ are the minimum and maximum asymtotes, respectively, \textit{Q} relates to the inflection point, $\omega$ is the rate of increase, \textit{L} is the lateral shift in the curve along x-axis, and $\nu$ affects the relative rates of increase near the two asymptotes. These parameters are set in params.

Stoat populations do not crash as fast as rodent populations following a mast. To account for this, we implement the following procedure. First, recall that a mast in year $t-1$ results in a spike in rodent population in year \textit{t} (in the absence of 1080 operation). In year $t+1$, the rodent population crashes due to density dependence, but the stoat population decreases more slowly. Therefore, in year $t+1$ following a mast in year $t-1$, we calculate $K^{[S]}_{k,t+1}$ as a function  of the mean $R_{k,t}$ and $R_{k,t+1}$ (using eq. \ref{eq:genLogistic}).

We set an initial stoat population density at $t=0$ ($S_{k,0}$) for all grid cells using random variate from a Poisson distribution with a mean equal to 0.75 of $K^{[S]}_{k,0}$:

\begin{equation}
S_{k,0} \sim Poisson(0.75 * K^{[s]}_{k,0})
\end{equation}

For subsequent time steps ($t>0$), the stoat population will grow with a Ricker population growth model in a similar process as used for rodents (eq. \ref{eq:rodentMU}). As with rodents, prior to population growth, the reproductive stoat population ($S^*_{k,t}$; note the * superscript) is calculated by adjusting $S_{k,t-1}$ for immigrants and emigrants from \textit{t-1}, and mortality due to eating toxic rodents if 1080 is applied in grid cell \textit{k} ($C_{i \in k, t}$). First, immigratation and emigration are incorporated as follows:

\begin{equation}
S^*_{k,t} = S_{k,t-1} + Imm^{[S]}_{k,t-1} - Em^{[S]}_{k,t-1}
\end{equation}

\noindent
where $Imm^{[S]}_{k,t-1}$ is the number of immigrants into cell \textit{k} from the previous year \textit{t-1}, and $Em^{[S]}_{k,t-1}$ is the number emigrants from cell \textit{k} in year \textit{t-1} to other cells. If 1080 is applied, the $S^*_{k,t}$ is reduced as follows:

\begin{equation}
S^*_{k,t} = S^*_{k,t} - T^{[S]}_{k,t}.
\end{equation}

\noindent
where $T^{[S]}_{k,t}$ is the the number of stoats in grid cell \textit{k} that eat poisoned rodents, become 'toxic stoats' and die.

The $T^{[S]}_{k,t}$ is a function of the the total number of toxic rodents in grid cell \textit{k} ($T^{[R]}_{k,t}$). We already calculated the number of toxic rodents at the fine-scale resolution ($T^{[R]}_{i,t}$; eq. \ref{eq:toxicRodent}), which we use to calculate  $T^{[R]}_{k,t}$ (note the \textit{k} subscript for the stoat resolution).

\begin{equation}
T^{[R]}_{k,t}= \sum_{i \in k} T^{[R]}_{i,t}.
\end{equation}

We then calculate the number of stoats in grid cell \textit{k} that eat toxic rodents and die of poisoning ($T^{[S]}_{k,t}$):

\begin{equation}
T^{[S]}_{k,t} = S^*_{k,t}Pr(eat|enc)Pr(enc)
\end{equation}

\noindent
where $Pr(eat|enc)$ is the probability of an individual stoat eating a toxic rodent given that it encounters (or captures) one (specified in params, \textit{e.g.} 0.8), and $Pr(enc)_{k,t}$ is the probability of encountering a toxic rodent. The $Pr(enc)_{k,t}$ is calculated as:

\begin{equation}
Pr(enc)_{k,t} = 1.0 - \exp(-encRate * T^{[R]}_{k,t})
\end{equation}

\noindent
where $encRate$ is the encounter rate (set in params; \textit{e.g.} 0.02) that links the $T^{[R]}_{k,t}$ to the $Pr(enc)$. The relationship between $T^{[R]}_{k,t}$ and the stoat $Pr(eat|enc)Pr(enc)$ is shown in Figure \ref{fig:pToxicStoat}. 

The Ricker population growth model is then calculated as follows:

\begin{equation}
S_{k,t} \sim Poisson (\mu^{[S]}_{k,t})
\end{equation}

\begin{equation}\label{eq:stoatMU}
\mu^{[S]}_{k,t} =  S^*_{k,t}\exp\left(r^{[S]}\left(1 - \frac{S^*_{k,t}}{K^{[S]}_{k,t}}\right)\right)
\end{equation}

\noindent
where $r^{[S]}$ is the stoat intrinsic rate of growth. 

The number of stoats that emigrate out of cell \textit{k} and year \textit{t} ($nEm^{[S]}_{k,t}$) is a random variate from a binomial distribution:

\begin{equation}
nEm^{[S]}_{k,t} \sim Binomial(S_{k,t}, \, pEm^{[S]}_{k,t})
\end{equation}

\noindent
where $pEm^{[S]}_{k,t}$ is the probability that an individual will emigrate from cell \textit{k} in year \textit{t}. The $pEm^{[S]}_{k,t}$ will depend on the current population density $(S_{k,t})$ relative to $K^{[S]}_{k,t}$ so that the probability will increase with increasing density (similar to rodents; see Figure \ref{fig:pEmigraion}): 

\begin{equation}
pEm^{[S]}_{k,t} = 1.0 - \exp\left(-\gamma^{[S]}\frac{(S_{k,t})}{K^{[S]}_{k,t}}\right)
\end{equation}

\noindent
where $\gamma^{[S]}$ is a rate parameter set in params (\textit{e.g.} 0.30).

The $nEm^{[S]}_{k,t}$ emigrants are then distributed into a window of \textit{L} cells that are in neighbourhood cells around \textit{k}. The n x n dimensions of the window are determined by the resolution and the maximum dispersal distance  of stoats ($maxDisp^{[K]}$). The destination cell \textit{l} of emigrants is determined by the distance from cell \textit{k} and the habitat quality or carrying capacity in cell \textit{l} $(K^{[S]}_{l,t})$. The $nEm^{[S]}_{k,t}$ are distributed with a multinomial process:

\begin{equation}
nEm^{[S]}_{k,l,t} \sim multinomial(nEm^{[S]}_{k,t}, \, pEm^{[S]}_{k,l,t})
\end{equation}

\noindent
where $nEm^{[S]}_{k,l,t}$ is the number of emigrants from cell \textit{k} into cell \textit{l} and $pEm^{[S]}_{k,l,t}$ is the probability of a disperser emigrating from cell \textit{k} into cell \textit{l}. 

The $pEm^{[S]}_{k,l,t}$ is calculated in a two-step process. First, the relative probability of emigrating from \textit{k} to \textit{l} is calculated as a function of distance between the cells $(d_{k,l})$ and the density ($S_{l,t}$) relative to $K^{[S]}_{l,t}$:

\begin{equation}
relProbEm^{[S]}_{k,l,t} =  \exp \left(-\frac{\tau^{[S]}S_{l,t}}{K^{[S]}_{l,t}} \right) \exp\left(\frac{-d_{k,l}}{\delta^{[S]}}\right)
\end{equation}


\noindent
where $\delta^{[S]}$ is a decay parameter describing the decreasing probability of immigrating into cell \textit{j} with increasing distance (\textit{e.g.} $\approx$ 0.25 of maximum dispersal distance), and $\tau^{[S]}$ is a rate parameter (\textit{e.g.} 1.4). Second, the absolute probability of emigrating from cell \textit{k} into cell \textit{l} is calculated by normalising all the $relProbEm^{[S]}_{k,l,t}$ of \textit{L} cells so that they sum to one:

\begin{equation}
pEm^{[S]}_{k,l,t} = \frac{relProbEm^{[S]}_{k,l,t}}{\sum_{l=1}^{L} relProbEm^{[S]}_{k,l,t}}
\end{equation}







\noindent
\textbf{Kiwi population process}\\

For each grid cell \textit{k} and time \textit{t} we draw a random variate for the kiwi population $B_{k,t}$ from a Poisson distribution with mean $\mu^{[B]}_{k,t}$:

\begin{equation}
B_{k,t} \sim Poisson (\mu^{[B]}_{k,t})
\end{equation}

\noindent
We obtain $\mu^{[B]}_{k,t}$ with logistic density dependent model at same resolutions used for stoats (\textit{e.g.} 1000 m). Note that we use '\textit{B}' (for Birds) to indicate kiwi and not \textit{K}, which is used for carrying capacity. 

The population growth model for kiwis uses a fixed carrying capacity $K^{[B]}$ for all grid cells and years, because the resource base is assumed to be stable over space and time, with the exception that $K^{[B]}$ is set to zero at elevation $>$ 1500 m (\textbf{I have to confirm this elevation}).


%%
%Because kiwi are susceptible to predation by stoats, (especially juveniles), we calculate a realised population growth rate $r^{[B]}_{k,t}$ by modifying the maximum intrinsic growth rate $r^{[B]}_{max}$ as a function of stoat density ($S_{k,t}$) in cell \textit{k} at time \textit{t}. That is, the $r^{[B]}_{k,t}$ will vary over space and time (Figure. \ref{fig:realkiwigrowth}). The realised growth rate will range from a minimum ($r^{[B]}_{min}$) of -0.04 to a $r^{[B]}_{max}$ of 0.10. The following equation is used to obtain the $r^{[B]}_{k,t}$ from a $S_{k,t}$ value: 

%\begin{equation}
%r^{[B]}_{k,t} = r^{[B]}_{min} + e^{-\psi S_{k,t}}(r^{[B]}_{max} - r^{[B]}_{min}) 
%\end{equation}

%\noindent
%where $\psi$ is a rate parameter (set in params, \textit{e.g.} 0.5) linking $S_{k,t}$ to the realised population growth in cell \textit{k} at time \textit{t}. 
%
Prior to population growth, the reproductive kiwi population ($B^*_{k,t}$; note the * superscript) is calculated by adjusting $B_{k,t-1}$ for immigrants and emigrants from \textit{t-1}. Immigration and emigration are incorporated as follows:

\begin{equation}
B^*_{k,t} = B_{k,t-1} + Imm^{[B]}_{k,t-1} - Em^{[B]}_{k,t-1}
\end{equation}

\noindent
where $Imm^{[B]}_{k,t-1}$ is the number of kiwi immigrants into cell \textit{k} from the previous year \textit{t-1}, and $Em^{[B]}_{k,t-1}$ is the number emigrants from cell \textit{k} in year \textit{t-1} to other cells.

The kiwi population will grow with a Ricker population growth model in a similar process as used for rodents and stoats (eq. \ref{eq:rodentMU} and \ref{eq:stoatMU}). The expected kiwi density at time \textit{t} ($\mu^{[B]}_{i,t}$) is then calculated as follows:

\begin{equation}\label{eq:kiwiMU}
\mu^{[B]}_{k,t} =  B^*_{k,t}\exp\left(r^{[B]}\left(1 - \frac{B^*_{k,t}}{K^{[B]}}\right)\right)\exp\left(-\psi S_{k,t}\right)
\end{equation}

\noindent
where $r^{[B]}$ is the kiwi intrinsic growth rate, and $\psi$ is rate parameter relating kiwi survival to stoat population density ($S_{k,t}$) and predation (\textit{e.g.} 0.05).

 





The number of kiwis that emigrate out of cell \textit{k} and year \textit{t} ($nEm^{[B]}_{k,t}$) is a random variate from a binomial distribution:

\begin{equation}
nEm^{[B]}_{k,t} \sim Binomial(B_{k,t}, \, pEm^{[B]}_{k,t})
\end{equation}

\noindent
where $pEm^{[B]}_{k,t}$ is the probability that an individual will emigrate from cell \textit{k} in year \textit{t}. The $pEm^{[B]}_{k,t}$ will depend on the current population density $(B_{k,t})$ relative to $K^{[B]}$ so that the probability will increase with increasing density: 

\begin{equation}
pEm^{[B]}_{k,t} = 1.0 - \exp\left(-\gamma^{[B]}\frac{(B_{k,t})}{K^{[B]}_{k,t}}\right)
\end{equation}

\noindent
where $\gamma^{[B]}$ is a rate parameter for kiwis set in params (\textit{e.g.} 0.30).

The $nEm^{[B]}_{k,t}$ emigrants are then distributed into a window of \textit{J} cells that are in neighbourhood cells around \textit{i}. The n x n dimensions of the window are determined by the resolution and the maximum dispersal distance  of kiwis ($maxDisp^{[B]}$). The destination cell \textit{j} of emigrants is determined by the distance from cell \textit{k} and the habitat quality or carrying capacity in cell \textit{j} $(K^{[B]}_{j,t})$. The $nEm^{[B]}_{k,t}$ are distributed with a multinomial process:

\begin{equation}
nEm^{[B]}_{k,j,t} \sim multinomial(nEm^{[B]}_{k,t}, \, pEm^{[B]}_{k,j})
\end{equation}

\noindent
where $nEm^{[B]}_{k,j,t}$ is the number of emigrants from cell \textit{i} into cell \textit{j} and $pEm^{[B]}_{k,j}$ is the probability of a disperser emigrating from cell \textit{k} into cell \textit{j}. 

The $pEm^{[B]}_{k,j}$ is calculated in a two-step process. First, the relative probability of emigrating from \textit{k} to \textit{j} is calculated as a function of distance between the cells $(d_{k,j})$ and the density ($B_{j,t}$) relative to $K^{[B]}_{j,t}$:

\begin{equation}
relProbEm^{[B]}_{k,j} =  \exp \left(- \frac{\tau^{[B]}B_{j,t}}{K^{[B]}_{j,t}} \right) \exp\left(\frac{-d_{k,j}}{\delta^{[B]}}\right)
\end{equation}


\noindent
where $\delta^{[B]}$ is a decay parameter describing the decreasing probability of immigrating into cell \textit{j} with increasing distance (\textit{e.g.} $\approx$ 0.25 of maximum dispersal distance), and $\tau^{[B]}$ is a rate parameter (\textit{e.g.} 1.4). Second, the absolute probability of emigrating from cell \textit{k} into cell \textit{j} is calculated by normalising all the $relProbEm^{[B]}_{k,j}$ of \textit{J} cells so that they sum to one:

\begin{equation}
pEm^{[B]}_{k,j} = \frac{relProbEm^{[B]}_{k,j}}{\sum_{j=1}^{J} relProbEm^{[B]}_{k,j}}
\end{equation}




\newpage
\bibliography{/home/dean/AA_Mega_Local/BibTeXLibrary/DeanBibTeX.bib}
\bibliographystyle{/home/dean/AA_Mega_Local/BibTeXLibrary/ecology.bst}






\end{document}
